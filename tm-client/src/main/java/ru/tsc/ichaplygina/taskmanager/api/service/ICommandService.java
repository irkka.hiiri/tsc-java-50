package ru.tsc.ichaplygina.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.util.List;
import java.util.Map;

public interface ICommandService {

    @NotNull Map<String, AbstractCommand> getArguments();

    @NotNull List<AbstractCommand> getCommandList();

    @NotNull Map<String, AbstractCommand> getCommands();

}
