package ru.tsc.ichaplygina.taskmanager.api.property;

import org.jetbrains.annotations.NotNull;

public interface IPasswordProperty {

    @NotNull Integer getPasswordIteration();

    @NotNull String getPasswordSecret();

}
