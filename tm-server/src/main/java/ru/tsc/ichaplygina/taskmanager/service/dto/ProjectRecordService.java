package ru.tsc.ichaplygina.taskmanager.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.dto.IProjectRecordRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IProjectRecordService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IUserRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.repository.dto.ProjectRecordRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public final class ProjectRecordService extends AbstractBusinessEntityRecordService<ProjectDTO> implements IProjectRecordService {

    public ProjectRecordService(@NotNull final IConnectionService connectionService, @NotNull final IUserRecordService userService) {
        super(connectionService, userService);
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final ProjectDTO project) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO(name, description, userId);
        add(project);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectDTO> projectList) {
        if (projectList == null) return;
        for (final ProjectDTO project : projectList) add(project);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.clear();
            else repository.clearForUser(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO completeById(@NotNull final String userId, @Nullable final String projectId) {
        return updateStatus(userId, projectId, Status.COMPLETED);
    }

    @Nullable
    @Override
    public ProjectDTO completeByIndex(@NotNull final String userId, final int index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) :
                    repository.getIdByIndexForUser(userId, index);
            return completeById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(name) :
                    repository.getIdByNameForUser(userId, name);
            if (isEmptyString(id)) return null;
            return completeById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll() : repository.findAllForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<ProjectDTO> comparator = getComparator(sortBy);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                    repository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return repository.findById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.findById(projectId) : repository.findByIdForUser(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.findByIndex(entityIndex) : repository.findByIndexForUser(userId, entityIndex);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.findByName(entityName) : repository.findByNameForUser(userId, entityName);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) : repository.getIdByIndexForUser(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(entityName) : repository.getIdByNameForUser(userId, entityName);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            return userService.isPrivilegedUser(userId) ?
                    repository.getSize() : repository.getSizeForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            @Nullable final ProjectDTO project = findById(id);
            if (project == null) return null;
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @Nullable final ProjectDTO project = findById(userId, id);
            if (project == null) return null;
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeById(id);
            else repository.removeByIdForUser(userId, id);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @Nullable final ProjectDTO project = findByIndex(userId, index);
            if (project == null) return null;
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeByIndex(index);
            else repository.removeByIndexForUser(userId, index);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @Nullable final ProjectDTO project = findByName(userId, name);
            if (project == null) return null;
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            if (userService.isPrivilegedUser(userId)) repository.removeByName(name);
            else repository.removeByNameForUser(userId, name);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO startById(@NotNull final String userId, @Nullable final String projectId) {
        return updateStatus(userId, projectId, Status.IN_PROGRESS);
    }

    @Nullable
    @Override
    public ProjectDTO startByIndex(@NotNull final String userId, final int index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            if (isInvalidListIndex(index, repository.getSize())) throw new IndexIncorrectException(index);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByIndex(index) :
                    repository.getIdByIndexForUser(userId, index);
            return startById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO startByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            @Nullable final String id = userService.isPrivilegedUser(userId) ?
                    repository.getIdByName(name) :
                    repository.getIdByNameForUser(userId, name);
            return startById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO updateById(@NotNull final String userId,
                                 @NotNull final String projectId,
                                 @NotNull final String projectName,
                                 @Nullable final String projectDescription) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (isEmptyString(projectName)) throw new NameEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final ProjectDTO project = findById(userId, projectId);
        if (project == null) return null;
        try {
            project.setName(projectName);
            project.setDescription(projectDescription);
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO updateByIndex(@NotNull final String userId,
                                    final int entityIndex,
                                    @NotNull final String entityName,
                                    @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

    @Nullable
    private ProjectDTO updateStatus(@NotNull final String userId, @Nullable final String projectId, @NotNull final Status status) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final ProjectDTO project = findById(userId, projectId);
        if (project == null) return null;
        try {
            project.setStatus(status);
            @NotNull final IProjectRecordRepository repository = new ProjectRecordRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
