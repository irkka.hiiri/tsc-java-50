package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ISessionService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ITaskService;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService(name = "TaskEndpoint")
public final class TaskEndpoint {

    @NotNull
    private final IProjectTaskService projectTaskService;
    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull final ITaskService taskService,
                        @NotNull final IProjectTaskService projectTaskService,
                        @NotNull final ISessionService sessionService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void addTaskToProject(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "projectId") @NotNull final String projectId,
                                 @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectTaskService.addTaskToProject(session.getUser().getId(), projectId, taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void clearTasks(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        taskService.clear(session.getUser().getId());
    }

    @WebMethod
    public void completeTaskById(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.completeById(session.getUser().getId(), taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void completeTaskByIndex(@WebParam(name = "session") @Nullable final Session session,
                                    @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.completeByIndex(session.getUser().getId(), taskIndex))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void completeTaskByName(@WebParam(name = "session") @Nullable final Session session,
                                   @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.completeByName(session.getUser().getId(), taskName))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void createTask(@WebParam(name = "session") @Nullable final Session session,
                           @WebParam(name = "taskName") @NotNull final String taskName,
                           @WebParam(name = "taskDescription") @Nullable final String taskDescription) {
        sessionService.validateSession(session);
        taskService.add(session.getUser().getId(), taskName, taskDescription);
    }

    @WebMethod
    public Task findTaskById(@WebParam(name = "session") @Nullable final Session session,
                             @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        return taskService.findById(session.getUser().getId(), taskId);
    }

    @WebMethod
    public Task findTaskByIndex(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        return taskService.findByIndex(session.getUser().getId(), taskIndex);
    }

    @WebMethod
    public Task findTaskByName(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        return taskService.findByName(session.getUser().getId(), taskName);
    }

    @WebMethod
    public List<Task> getTaskList(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "sortBy") @Nullable final String sortBy) {
        sessionService.validateSession(session);
        return taskService.findAll(session.getUser().getId(), sortBy);
    }

    @WebMethod
    public List<Task> getTaskListByProject(@WebParam(name = "session") @Nullable final Session session,
                                           @WebParam(name = "projectId") @NotNull final String projectId,
                                           @WebParam(name = "sortBy") @Nullable final String sortBy) {
        sessionService.validateSession(session);
        return projectTaskService.findAllTasksByProjectId(session.getUser().getId(), projectId, sortBy);
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.removeById(session.getUser().getId(), taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void removeTaskByIndex(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.removeByIndex(session.getUser().getId(), taskIndex))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void removeTaskByName(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.removeByName(session.getUser().getId(), taskName))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void removeTaskFromProject(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "projectId") @NotNull final String projectId,
                                      @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        projectTaskService.removeTaskFromProject(session.getUser().getId(), projectId, taskId);
    }

    @WebMethod
    public void startTaskById(@WebParam(name = "session") @Nullable final Session session,
                              @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.startById(session.getUser().getId(), taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void startTaskByIndex(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.startByIndex(session.getUser().getId(), taskIndex))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void startTaskByName(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.startByName(session.getUser().getId(), taskName))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void updateTaskById(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "taskId") @NotNull final String taskId,
                               @WebParam(name = "taskName") @NotNull final String taskName,
                               @WebParam(name = "taskDescription") @Nullable final String taskDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.updateById(session.getUser().getId(), taskId, taskName, taskDescription))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void updateTaskByIndex(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "taskIndex") final int taskIndex,
                                  @WebParam(name = "taskName") @NotNull final String taskName,
                                  @WebParam(name = "taskDescription") @Nullable final String taskDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.updateByIndex(session.getUser().getId(), taskIndex, taskName, taskDescription))
                .orElseThrow(TaskNotFoundException::new);
    }

}
