package ru.tsc.ichaplygina.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;

import java.util.List;

public interface IProjectRecordRepository extends IAbstractBusinessEntityRecordRepository<ProjectDTO> {

    void clear();

    void clearForUser(@NotNull String userid);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllForUser(@NotNull String userId);

    @Nullable
    ProjectDTO findById(@NotNull String id);

    @Nullable
    ProjectDTO findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO findByIndex(int index);

    @Nullable
    ProjectDTO findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    ProjectDTO findByName(@NotNull String name);

    @Nullable
    ProjectDTO findByNameForUser(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByIndex(int index);

    @Nullable
    String getIdByIndexForUser(@NotNull String userId, int index);

    @Nullable
    String getIdByName(@NotNull String name);

    @Nullable
    String getIdByNameForUser(@NotNull String userId, @NotNull String name);

    long getSize();

    long getSizeForUser(@NotNull String userId);

    void removeById(@NotNull String id);

    void removeByIdForUser(@NotNull String userId, @NotNull String id);

    void removeByIndex(int index);

    void removeByIndexForUser(@NotNull String userId, int index);

    void removeByName(@NotNull String name);

    void removeByNameForUser(@NotNull String userId, @NotNull String name);

}
