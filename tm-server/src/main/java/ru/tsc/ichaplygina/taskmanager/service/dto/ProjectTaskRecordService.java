package ru.tsc.ichaplygina.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IProjectRecordService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.IProjectTaskRecordService;
import ru.tsc.ichaplygina.taskmanager.api.service.dto.ITaskRecordService;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public final class ProjectTaskRecordService implements IProjectTaskRecordService {

    @NotNull
    private final IProjectRecordService projectService;

    @NotNull
    private final ITaskRecordService taskService;

    public ProjectTaskRecordService(@NotNull final ITaskRecordService taskService, @NotNull final IProjectRecordService projectService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Nullable
    @Override
    public final TaskDTO addTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        return (taskService.addTaskToProject(userId, taskId, projectId));
    }

    @Override
    public final void clearProjects(@NotNull final String userId) {
        projectService.findAll(userId).forEach(project -> taskService.removeAllByProjectId(project.getId()));
        projectService.clear(userId);
    }

    @NotNull
    @Override
    public final List<TaskDTO> findAllTasksByProjectId(@NotNull final String userId,
                                                       @NotNull final String projectId,
                                                       @NotNull final String sortBy) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        return taskService.findAllByProjectId(userId, projectId, sortBy);
    }

    @Nullable
    @Override
    public final ProjectDTO removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        taskService.removeAllByProjectId(projectId);
        return projectService.removeById(userId, projectId);
    }

    @Nullable
    @Override
    public final ProjectDTO removeProjectByIndex(@NotNull final String userId, final int projectIndex) {
        if (isInvalidListIndex(projectIndex, projectService.getSize(userId)))
            throw new IndexIncorrectException(projectIndex + 1);
        return removeProjectById(userId, Optional.ofNullable(projectService.getId(userId, projectIndex)).orElseThrow(ProjectNotFoundException::new));
    }

    @Nullable
    @Override
    public final ProjectDTO removeProjectByName(@NotNull final String userId, @NotNull final String projectName) {
        if (isEmptyString(projectName)) throw new NameEmptyException();
        return removeProjectById(userId, Optional.ofNullable(projectService.getId(userId, projectName)).orElseThrow(ProjectNotFoundException::new));
    }

    @Nullable
    @Override
    public final TaskDTO removeTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        Optional.ofNullable(projectService.findById(userId, projectId)).orElseThrow(ProjectNotFoundException::new);
        return (taskService.removeTaskFromProject(userId, taskId, projectId));
    }

}
