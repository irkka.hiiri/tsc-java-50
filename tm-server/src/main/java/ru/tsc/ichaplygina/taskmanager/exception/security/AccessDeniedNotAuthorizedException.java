package ru.tsc.ichaplygina.taskmanager.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class AccessDeniedNotAuthorizedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Operation unavailable for non-authorized users.";

    public AccessDeniedNotAuthorizedException() {
        super(MESSAGE);
    }

}
