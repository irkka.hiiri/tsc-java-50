package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.bootstrap.Bootstrap;

public class TaskManagerLogger {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
